import React, { useState } from 'react';
import ControlPanel from './components/control-panel/ControlPanel.js';
import Scene from './components/scene/Scene.js';
import './App.css';

// method for forcing React component update on functional App component with mock hook
function ForceUpdate() {
  const [value, setValue] = useState(0);
  return () => setValue((value) => ++value);
}

// The parent app component dictates communication between the control panels and the
function App() {
  const [controlMode, setControlMode] = useState(null);
  const [mesh, setMesh] = useState(null);
  const forceUpdate = ForceUpdate();

  return (
    <div className="App">
      <ControlPanel setControlMode={setControlMode} setMesh={setMesh} forceAppUpdate={forceUpdate} />
      <Scene controlMode={controlMode} mesh={mesh} setMesh={setMesh} />
    </div>
  );
}

export default App;
