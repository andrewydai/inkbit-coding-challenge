import React from 'react';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { TransformControls } from 'three/examples/jsm/controls/TransformControls.js';
import autoBind from 'auto-bind';

/*
This component creates the THREE.js scene, with specifications. It also adjusts the scene when the parent
component dictates. An important distinction is that much of the changese are directly to THREE components
versus calling for rerender on the React component. 
*/
export default class Scene extends React.Component {
  constructor(props) {
    super(props);

    // establishese class variables and calls setup functions to fill in scene specifications
    this.renderer = new THREE.WebGLRenderer();
    this.currentCamera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.01, 30000);
    this.scene = new THREE.Scene();
    this.orbit = new OrbitControls(this.currentCamera, this.renderer.domElement);
    this.control = new TransformControls(this.currentCamera, this.renderer.domElement);
    this.setUpRenderer();
    this.setUpCurrentCamera();
    this.setUpScene();
    this.setUpOrbit();
    this.setUpControl();
    this.setUpListeners();
    this.renderScene();

    autoBind(this);
  }

  // Adds renderer to DOM in so as not to be added more than once
  componentDidMount() {
    document.body.appendChild(this.renderer.domElement);
  }

  // Whenever the parent dictates a prop change, change the scene.
  componentDidUpdate() {
    this.props.controlMode && this.handleKeyDownEvent(this.props.controlMode);
    if (this.props.mesh) {
      this.scene.add(this.props.mesh);
      this.control.attach(this.props.mesh);
      // make sure we don't add the same mesh multiple times on parent update
      this.props.setMesh(null);
    }
  }

  setUpRenderer() {
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.shadowMap.enabled = true;
  }

  setUpCurrentCamera() {
    this.currentCamera.position.set(100, 150, 300);
    this.currentCamera.lookAt(0, 0, 0);
  }

  setUpScene() {
    this.scene.background = new THREE.Color(0xcccccc);
    this.scene.add(new THREE.GridHelper(400, 20, 0x888888, 0x444444));
    const plane = new THREE.Mesh(
      new THREE.PlaneBufferGeometry(400, 400),
      new THREE.MeshPhongMaterial({ color: 0x999999, specular: 0x101010 })
    );
    plane.rotation.x = -Math.PI / 2;
    plane.position.y = -0.5;
    plane.receiveShadow = true;
    this.scene.add(plane);

    //lights
    this.scene.add(new THREE.HemisphereLight(0x443333, 0x111122));
    this.addShadowedLight(1, 200, 1, 0xdddddd, 1.35);
    this.addShadowedLight(0.5, 100, -1, 0xdd8800, 1);
  }

  setUpOrbit() {
    this.orbit.update();
    this.orbit.addEventListener('change', () => this.renderScene());
  }

  setUpControl() {
    this.control.addEventListener('change', () => this.renderScene());
    this.control.addEventListener('dragging-changed', (event) => {
      this.orbit.enabled = !event.value;
    });
    this.scene.add(this.control);
  }

  setUpListeners() {
    window.addEventListener('resize', () => this.onWindowResize(), false);
    window.addEventListener('keydown', (event) => this.handleKeyDownEvent(event.keyCode));
    window.addEventListener('keyup', (event) => {
      switch (event.keyCode) {
        case 16: // Shift
          this.control.setTranslationSnap(null);
          this.control.setRotationSnap(null);
          this.control.setScaleSnap(null);
          break;
      }
    });
  }

  addShadowedLight(x, y, z, color, intensity) {
    const directionalLight = new THREE.DirectionalLight(color, intensity);
    directionalLight.position.set(x, y, z);
    this.scene.add(directionalLight);
    directionalLight.castShadow = true;
    const d = 400;
    directionalLight.shadow.camera.left = -d;
    directionalLight.shadow.camera.right = d;
    directionalLight.shadow.camera.top = d;
    directionalLight.shadow.camera.bottom = -d;
    directionalLight.shadow.camera.near = 1;
    directionalLight.shadow.camera.far = 400;
    directionalLight.shadow.bias = -0.002;
  }

  onWindowResize() {
    this.currentCamera.aspect = window.innerWidth / window.innerHeight;
    this.currentCamera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderScene();
  }

  handleKeyDownEvent(keyCode) {
    switch (keyCode) {
      case 81: // Q
        this.control.setSpace(this.control.space === 'local' ? 'world' : 'local');
        break;

      case 16: // Shift
        this.control.setTranslationSnap(100);
        this.control.setRotationSnap(THREE.MathUtils.degToRad(15));
        this.control.setScaleSnap(0.25);
        break;

      case 87: // W
        this.control.setMode('translate');
        break;

      case 69: // E
        this.control.setMode('rotate');
        break;

      case 82: // R
        this.control.setMode('scale');
        break;

      case 187:
      case 107: // +, =, num+
        this.control.setSize(this.control.size + 0.1);
        break;

      case 189:
      case 109: // -, _, num-
        this.control.setSize(Math.max(this.control.size - 0.1, 0.1));
        break;

      case 88: // X
        this.control.showX = !this.control.showX;
        break;

      case 89: // Y
        this.control.showY = !this.control.showY;
        break;

      case 90: // Z
        this.control.showZ = !this.control.showZ;
        break;

      case 32: // Spacebar
        this.control.enabled = !this.control.enabled;
        break;
    }
  }

  renderScene() {
    this.renderer.render(this.scene, this.currentCamera);
  }

  render() {
    return <div />;
  }
}
