import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { Layout, Button, Upload, message, Modal } from 'antd';
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader.js';
import * as THREE from 'three/build/three.module.js';
const { Header } = Layout;
const loader = new STLLoader();
const reader = new FileReader();
// global variable to keep track of file names being uploaded
let fileNameBeingUploaded = '';

/*
This component is the control panel of thee webapp, and allows the user to both control the shape
currently attachede to, and add or load new shapes.
*/
export default function ControlPanel(props) {
  // react hooks to keep track of meshFiles uploaded and the stl mesh loading pop-up
  const [meshFiles, setMeshFiles] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  // callbacks for the file reader when reading new files
  reader.onload = (e) => {
    message.success(`"${fileNameBeingUploaded}" file uploaded successfully`);
    meshFiles.push({ fileName: fileNameBeingUploaded, ArrayBuffer: e.target.result });
    fileNameBeingUploaded = '';
    setMeshFiles(meshFiles);
  };
  reader.onerror = (error) => {
    message.error(`An error occurred while uploading "${fileNameBeingUploaded}"`);
    fileNameBeingUploaded = '';
  };

  // callbacks for buttons in stl mesh loader pop-up
  const handleModalOk = () => setModalVisible(false);
  const handleModalCancel = () => setModalVisible(false);

  // grab parent callback props for changing scene and controls
  const { setControlMode, setMesh, forceAppUpdate } = props;
  const setControlModeDefault = (keyCode) => setControlMode(keyCode);
  const setControlModeForceUpdate = (keyCode) => {
    setControlMode(keyCode);
    forceAppUpdate();
  };

  return (
    <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
      <div className="control-button-container">
        <Button type="default" size="small" onClick={() => setControlModeDefault(87)}>
          "W" translate
        </Button>
        <Button type="default" size="small" onClick={() => setControlModeDefault(69)}>
          "E" rotate
        </Button>
        <Button type="default" size="small" onClick={() => setControlModeDefault(82)}>
          "R" scale
        </Button>
        <Button type="default" size="small" onClick={() => setControlModeDefault(107)}>
          "+" to increase size
        </Button>
        <Button type="default" size="small" onClick={() => setControlModeDefault(109)}>
          "-" to decrease size
        </Button>
        <Button type="default" size="small" onClick={() => setControlModeForceUpdate(81)}>
          "Q" toggle world/local space
        </Button>
        <Button type="default" size="small" onClick={() => setControlModeDefault(16)}>
          "Shift" snap to grid
        </Button>

        <Button type="default" size="small" onClick={() => setControlModeForceUpdate(88)}>
          "X" toggle X
        </Button>
        <Button type="default" size="small" onClick={() => setControlModeForceUpdate(89)}>
          "Y" toggle Y
        </Button>
        <Button type="default" size="small" onClick={() => setControlModeForceUpdate(90)}>
          "Z" toggle Z
        </Button>
        <Button type="default" size="small" onClick={() => setControlModeForceUpdate(32)}>
          "Spacebar" toggle enabled
        </Button>
      </div>
      <div className="stl-button-container">
        <Button type="primary" onClick={() => setModalVisible(true)}>
          Load STL File
        </Button>
        <Modal title="STL Files" visible={modalVisible} onOk={handleModalOk} onCancel={handleModalCancel}>
          {meshFiles.map((meshFile) => {
            return (
              <Button
                type="default"
                onClick={() => {
                  const geometry = loader.parse(meshFile.ArrayBuffer);
                  const material = new THREE.MeshPhongMaterial({ color: 0xc0b0cc, specular: 0x111111, shininess: 200 });
                  const mesh = new THREE.Mesh(geometry, material);
                  mesh.position.set(0, 25, 0);
                  mesh.castShadow = true;
                  mesh.receiveShadow = true;
                  mesh.rotation.x = -Math.PI / 2;
                  setMesh(mesh);
                }}
              >
                {meshFile.fileName}
              </Button>
            );
          })}
        </Modal>
        <Upload
          type="file"
          accept=".stl"
          customRequest={dummyRequest}
          transformFile={(file) => stlUploadProgress(file, meshFiles)}
          fileList={[]}
        >
          <Button type="primary">Upload STL File</Button>
        </Upload>
      </div>
    </Header>
  );
}

// dummy request for antd upload component that automatically posts back a success
function dummyRequest({ file, onSuccess }) {
  setTimeout(() => {
    onSuccess('ok');
  }, 0);
}

// processes file being uploaded with file reader. Cannot upload files wtih same namee
function stlUploadProgress(file, meshFiles) {
  const previousFileNames = meshFiles.map((meshFile) => meshFile.fileName);
  if (previousFileNames.includes(file.name)) {
    message.error(`File with name "${file.name}" has already been uploaded`);
    return;
  }

  fileNameBeingUploaded = file.name;
  reader.readAsArrayBuffer(file);
}
